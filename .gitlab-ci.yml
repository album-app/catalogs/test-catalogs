stages:
  - test

.linux_base_template:
  image: continuumio/miniconda3:latest
  tags:
    - docker
  before_script:
    - apt install libxi6
    - python -V  # Print out python version for debugging
    - pwd
    - which python
    - conda install -y mamba -c conda-forge
    - conda create -y --channel=conda-forge -n album album python=3.10
    - conda init bash
    - source ~/.bashrc
    - conda activate album
  variables:
    PIP_CACHE_DIR: $CI_PROJECT_DIR/.cache/pip
    CONDA_ENV_NAME: album
    CONDA_PREFIX: /opt/conda
    PREFIX: $CONDA_PREFIX/envs/$CONDA_ENV_NAME
    ALBUM_BASE_CACHE_PATH: $CI_PROJECT_DIR/.album
  cache:
    key: one-key-to-rule-them-all-linux
    paths:
      - ${CONDA_PREFIX}/pkgs/*.tar.bz2
      - ${CONDA_PREFIX}/pkgs/urls.txt
      - .cache/pip

.macos_base_template:
  tags:
    - macos
    - shell
  before_script:
    - echo "$(uname)"
    - sw_vers
    - tmpdir=$(mktemp -d /tmp/album-test.XXXXXX)
    - echo $tmpdir
    - echo $tmpdir > /tmp/tmpdir
    - curl https://repo.anaconda.com/miniconda/Miniconda3-latest-MacOSX-x86_64.sh --output $tmpdir/miniconda.sh
    - bash $tmpdir/miniconda.sh -b -p $tmpdir/miniconda
    - export PATH=$PATH:$tmpdir/miniconda/bin/
    - export ALBUM_BASE_CACHE_PATH=$tmpdir/.album
    - echo $PATH
    - source $tmpdir/miniconda/bin/activate
    - conda install -y mamba -c conda-forge
    - conda create -y --channel=conda-forge -n album python=3.10 album
    - conda activate album
  after_script:
    - tmpdir=$(</tmp/tmpdir)
    - echo $tmpdir
    - rm -rf $tmpdir

.windows_base_template:
  before_script:
    - 'echo "We are in path: $pwd "'
    - 'echo "conda URL:  $env:MINICONDA_URL "'
    - '$oldProgressPreference = $progressPreference; $progressPreference = "SilentlyContinue";'
    - 'if(-Not (Test-Path .\download)) {echo "Cache download not found! Creating..."; New-Item -ItemType Directory -Force -Path .\download} else { echo ".\download cache found! with content:"; Get-ChildItem -Path .\download}'                                                                                                                                                # create cache dir
    - '[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12'                                                                                                                                                                                                                                                                                          # set Security download protocol to avoid https errors
    - 'if(Test-Path .\download\miniconda_url.txt) {echo "found previous miniconda url information. Extracting information..."; $miniconda_cache_url = try {Get-Content -Path .\download\miniconda_url.txt -errorAction Stop} catch {echo "None"}; echo "Cached minconda executable: $miniconda_cache_url"} else { echo ".\download\miniconda_url.txt does not exist!"}'          # read url from cache
    - 'if($miniconda_cache_url -ne $env:MINICONDA_URL) {echo "Cached URL not equal to given URL. Removing executable..."; Remove-Item -Path .\download\miniconda3.exe -Force; $force_install = "true"; echo "Force installation triggered!"} else { echo "No force installation necessary!"}'                                                                                    # check if url changed - if yes, remove cache and mark conda reinstall
    - 'if(-Not (Test-Path .\download\miniconda3.exe)) {echo "Downloading miniconda executable..."; Invoke-WebRequest -UseBasicParsing -Uri $env:MINICONDA_URL -OutFile .\download\miniconda3.exe} else {echo "Executable found in .\download\miniconda3.exe. Reusing..."}'                                                                                                       # download the miniconda windows executable
    - 'echo $env:MINICONDA_URL | Out-File -FilePath .\download\miniconda_url.txt'                                                                                                                                                                                                                                                                                                # cache version information
    - 'Get-ChildItem -Path .\download'                                                                                                                                                                                                                                                                                                                                           # show download folder content
    - '$env:PATH += ";$pwd\miniconda\condabin"'                                                                                                                                                                                                                                                                                                                                  # set path information
    - '$conda_available = try {Get-Command "Get-CondaEnvironment" -errorAction Stop} catch {$null}'                                                                                                                                                                                                                                                                              # check if conda cmnd already available
    - 'if($force_install -eq "true") {$conda_available = $null}'                                                                                                                                                                                                                                                                                                                 # mark conda reinstall
    - 'if($conda_available -eq $null) {echo "conda cmnd not available! Will install in $pwd\miniconda..."; Start-Process .\download\miniconda3.exe -argumentlist "/InstallationType=JustMe /S /D=$pwd\miniconda" -wait} else {echo "Skip downloading!..."}'
    - 'echo "Environment path: $env:PATH"'
    - 'conda init'
    - 'conda config --set notify_outdated_conda false'
    - 'conda install -y mamba -c conda-forge'
    - 'conda create -y -c conda-forge -n album python=3.10 album'
    - '$progressPreference = $oldProgressPreference'
  tags:
    - windows
  variables:
    PIP_CACHE_DIR: $CI_PROJECT_DIR\.cache\pip
    ErrorActionPreference: Continue  # not working properly
    MINICONDA_URL: https://repo.anaconda.com/miniconda/Miniconda3-latest-Windows-x86_64.exe
    ALBUM_BASE_CACHE_PATH: $CI_PROJECT_DIR\.album
  cache:
    key: one-key-to-rule-them-all-windows
    paths:
      - .\download
      - .cache\pip


test_solutions_linux:
  extends: .linux_base_template
  stage: test
  script:
    - album add-catalog https://gitlab.com/album-app/catalogs/helmholtz-imaging-dev
    - album install src/de.mdc-berlin/blender-import-meshes
    - album test src/de.mdc-berlin/blender-import-meshes

test_solutions_macos:
  extends: .macos_base_template
  stage: test
  script:
    - album add-catalog https://gitlab.com/album-app/catalogs/helmholtz-imaging-dev
    - album install src/de.mdc-berlin/blender-import-meshes
    - album test src/de.mdc-berlin/blender-import-meshes

test_solutions_windows:
  extends: .windows_base_template
  stage: test
  script:
    - $cmnd = powershell.exe -command {conda activate album 2>&1 | Write-Host; album add-catalog https://gitlab.com/album-app/catalogs/helmholtz-imaging-dev 2>&1 | Write-Host; album install src\de.mdc-berlin\blender-import-meshes 2>&1 | Write-Host; album test src\de.mdc-berlin\blender-import-meshes 2>&1 | Write-Host; exit(0)}
    - if ("Success" -in $cmnd[-2..-1]) {exit(0)} else {exit(1)}

